/*
MIT License

Copyright (c) 2019 Thomas Lovell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define DEBUG 1     /* '1' produces debug info; '0' does not */
#define _LARGEFILE64_SOURCE
#define S 512
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "hexDump.h"

int main(int argc, char **argv) {
int      ifd, ofd, ret, errsv;
char     ifn[S], ofn[S+5];
ssize_t  sz=0;
off64_t  off64t, of, sectors;
unsigned short int sig=43605; /* 0x55aa */
unsigned int p1_lba, p1_num, p1_end;
unsigned int p2_lba, p2_num, p2_end;
unsigned int p3_lba, p3_num, p3_end;
unsigned int p4_lba, p4_num, p4_end;

union rec_tag {
    struct mbr_tag {
        char zeros[440];
        int  diskid;
        char diskid2[2];
        struct part_tag {
            unsigned char status[1];
            unsigned char chs_first[3];
            unsigned char type[1];
            unsigned char chs_last[3];
            unsigned char lba[4];
            unsigned char num[4];
        } p1, p2, p3, p4;
        unsigned short int disksig;
    } mbr; 
    char    buf[S];
} rec;

    if (argc != 3) {
        printf("usage: raspberry-trunc-img { <device> | <input image file> } <output img file>\n\n");
        printf("   eg: raspberry-trunc-img /dev/sdb 2019-04-08-raspbian-stretch-full-lvm.img \n");
        printf("                                 - or - \n");
        printf("       raspberry-trunc-img big-file 2019-04-08-raspbian-stretch-full-lvm.img \n");
        exit(4);
    }
    strncpy(ifn, argv[1], sizeof(ifn)-1);
    printf("input  filename set to %s\n", ifn);
    strncpy(ofn, argv[2], sizeof(ofn)-1);
    printf("output filename set to %s\n", ofn);

    if ((ifd = open(ifn, O_RDONLY | O_LARGEFILE)) == -1) {
        errsv = errno;
        printf("The input file, '%s', could not be opened.\n", ifn);
        printf(" errno is '%i - %s'\n", errsv, strerror(errsv));
        exit(4);
    }

    if ((sz = read(ifd, (void *) rec.buf, S)) == S) {
        if (DEBUG) {
            hexDump("diskid  ", &rec.mbr.diskid, 4);
            hexDump("disksig ", &rec.mbr.disksig, 2);
            hexDump("0x55aa  ", &sig, 2);
        }
        if (memcmp(&rec.mbr.disksig, &sig, 2) != 0 ) {
            printf("Input file disk signature, '%2x', not 0x55aa.\n", rec.mbr.disksig);
            exit(4);
        }
    } else {
        printf("size read, %li, not %i\n", sz, S);
        exit(4);
    }

    memcpy(&p1_lba, &rec.mbr.p1.lba, 4);
    memcpy(&p1_num, &rec.mbr.p1.num, 4);
    p1_end = p1_lba + p1_num;
    memcpy(&p2_lba, &rec.mbr.p2.lba, 4);
    memcpy(&p2_num, &rec.mbr.p2.num, 4);
    p2_end = p2_lba + p2_num;
    memcpy(&p3_lba, &rec.mbr.p3.lba, 4);
    memcpy(&p3_num, &rec.mbr.p3.num, 4);
    p3_end = p3_lba + p3_num;
    memcpy(&p4_lba, &rec.mbr.p4.lba, 4);
    memcpy(&p4_num, &rec.mbr.p4.num, 4);
    p4_end = p4_lba + p4_num;

    printf("Partition 1: starting sector: %i; number of sectors: %i\n", p1_lba, p1_num);
    if (p2_end) printf("Partition 2: starting sector: %i; number of sectors: %i\n", p2_lba, p2_num);
    if (p3_end) printf("Partition 3: starting sector: %i; number of sectors: %i\n", p3_lba, p3_num);
    if (p4_end) printf("Partition 4: starting sector: %i; number of sectors: %i\n", p4_lba, p4_num);

    /* find last sector used according to the partition table */
    sectors=p1_end;
    if (sectors < p2_end) sectors = p2_end;
    if (sectors < p3_end) sectors = p3_end;
    if (sectors < p4_end) sectors = p4_end;

    if ((ofd = open(ofn, O_WRONLY | O_LARGEFILE | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1) {
        errsv = errno;
        printf("The output file '%s' could not be opened.\n", ofn);
        printf(" errno is '%i - %s'\n", errsv, strerror(errsv));
        exit(4);
    }
    write(ofd, rec.buf, S);  /* write mbr */

    /* decrement sectors before loop so we do one less */
    while (--sectors) {
        if (!(sz = (read(ifd, rec.buf, S)) == S)) {
            printf ("size read, %li, not %i\n", sz, S);
            exit(4);
        }
        write(ofd, rec.buf, S);
    }

    close(ofd);
 
    close(ifd);
    exit(0);
}