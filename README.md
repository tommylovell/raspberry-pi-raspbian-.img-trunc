# Raspberry Pi raspbian .img Trunc

A Utility to take an SD card / USB drive DD'd .img and truncate it at the end 
of the last MBR partition.

usage: raspberry-trunc-img { \<device\> | \<input image file\> } \<output img file\>

 eg: raspberry-trunc-img /dev/sdb 2019-04-08-raspbian-stretch-full-lvm.img 
 -or-
     raspberry-trunc-img big-file 2019-04-08-raspbian-stretch-full-lvm.img 

It will take an .img file or device that is "64" GB (or any size, for that 
matter) and produce an output .img file that is only as long as the last MBR 
partition, i.e. if the last partition ends at the 6GiB mark (usually -1), 
then the resulting output will only be 6GiB.